# Installation
> `npm install --save @types/caniuse-lite`

# Summary
This package contains type definitions for caniuse-lite (https://github.com/ben-eb/caniuse-lite#readme).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/caniuse-lite.

### Additional Details
 * Last updated: Mon, 20 Nov 2023 08:36:46 GMT
 * Dependencies: none

# Credits
These definitions were written by [Michael Utech](https://github.com/mutech).
